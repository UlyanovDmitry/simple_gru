from gensim.models import Word2Vec
import numpy as np

length = 200
min_count = 4
window = 5
max_vocabulary_size = 10000

UNK_token = 1
PAD_token = 0

def learn(path_to_save, data_rows):
    sentences = []
    from ast import literal_eval
    for row in data_rows:
        sentences.append(literal_eval(row))
        # if isinstance(row, str):
        #     if split_symbol in row:
        #         sentences.append(row.split(split_symbol))
        #     elif len(row) > 0:
        #         sentences.append([row, ])

    model = Word2Vec(sentences=sentences, size=length, window=window, min_count=min_count, workers=4, max_vocab_size=max_vocabulary_size)
    model.save(path_to_save)
    

def get_dict_from_model(path_to_load):
    m = Word2Vec.load(path_to_load)
    vocabulary = {"<PAD>": 0, "<UNK>": 1}
    unk_row = np.random.uniform(-1, 1, length).tolist()
    matrix = [[0] * length, unk_row]
    cur_index = 2
    for w in m.wv.vocab.keys():
        vocabulary[w] = cur_index
        cur_index += 1
        matrix.append(m.wv[w].tolist())

    return vocabulary, matrix



# import pandas
# texts = []
# df = pandas.read_csv("../../data/new_data/train_splitted.csv")
# texts = texts + df['comment_text'].tolist()
# df = pandas.read_csv("../../data/new_data/test_splitted.csv")
# texts = texts + df['comment_text'].tolist()
# learn("../../data/new_data/word2vec", data_rows=texts)

# model = Word2Vec.load("../../data/new_data/word2vec")
# print(len(model.wv.vocab.keys()))
# print(model.wv.vocab)
# while True:
#     try:
#         print('Input word:')
#         word = input()
#         print('Most similar to ' + '"' + word + '":')
#         res = model.wv.most_similar_cosmul(positive=[word])
#         for pair in res:
#             print(pair[0] + ' - ' + str(pair[1]))
#     except Exception as e:
#         print("----")

# voc, matr = get_dict_from_model("../../data/new_data/word2vec")
# import pandas
# from core.data_preprocessing.data_converter import tokenize
# text = pandas.read_csv("../../data/new_data/train_splitted.csv")
# text['comment_text'] = text['comment_text'].apply(lambda x: tokenize(x, voc, 1))
# for line in text['comment_text'][:20]:
#     print(line)
#     print('========================')
# text.to_csv('../../data/new_data/train_tokenized.csv', index=False)
