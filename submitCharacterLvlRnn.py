#!/usr/bin/python
from ast import literal_eval
import numpy as np
import tensorflow as tf
import pandas
import sys


def get_tokenized_rows(path, index_from, max_number_of_rows):
    d = pandas.read_csv(path, skiprows=index_from, header=None, nrows=max_number_of_rows, compression='zip')
    res = []
    for i in range(0, len(d.iloc[:, 0])):
        res.append([literal_eval(d.iloc[i, 0]),
                    [d.iloc[i, 1], d.iloc[i, 2], d.iloc[i, 3], d.iloc[i, 4], d.iloc[i, 5], d.iloc[i, 6]]])
    return res, len(d)


def create_batch(path, index_from, max_number_of_rows):
    _data, _len = get_tokenized_rows(path=path, index_from=index_from, max_number_of_rows=max_number_of_rows)
    _max_sequence_length = len(max(_data, key=lambda x: len(x[0]))[0])
    _inputs = np.zeros(shape=[batch_size, _max_sequence_length], dtype=np.int32)
    _target = np.zeros(shape=[batch_size, len(_data[0][1])], dtype=np.float32)
    _lengths = np.zeros(shape=[batch_size], dtype=np.int32)
    for i in range(0, len(_data)):
        _lengths[i] = len(_data[i][0])
        for j in range(0, len(_data[i][0])):
            _inputs[i][j] = _data[i][0][j]
        for k in range(0, len(_data[i][1])):
            _target[i][k] = _data[i][1][k]
    return _inputs, _target, _lengths, _len


def make_submission(session, path_to_file, path_to_save):
    result = pandas.DataFrame(columns=['id', 'toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate'])
    current_index = 1
    finished = False
    _nrows = 1024
    while not finished:
        test_df = pandas.read_csv(path_to_file, nrows=_nrows, header=None, skiprows=current_index)
        if len(test_df) < _nrows:
            finished = True
        _data = [literal_eval(row) for row in test_df.iloc[:, 1].tolist()]
        _ids = test_df.iloc[:, 0].tolist()
        _max_sequence_length = len(max(_data, key=lambda x: len(x)))
        _inputs = np.zeros(shape=[len(test_df), _max_sequence_length], dtype=np.int32)
        _lengths = np.zeros(shape=[len(test_df)], dtype=np.int32)
        for i in range(0, len(_data)):
            _lengths[i] = len(_data[i])
            for j in range(0, len(_data[i])):
                _inputs[i][j] = _data[i][j]

        _prediction = predict(session=session, inputs=_inputs, input_lengths=_lengths)

        for k in range(len(test_df)):
            result.loc[k + current_index - 1] = [_ids[k], _prediction[k][0], _prediction[k][1], _prediction[k][2],
                                                 _prediction[k][3], _prediction[k][4], _prediction[k][5]]
        current_index += len(test_df)
        print("Submission rows complete: {}".format(current_index - 1))

    result.to_csv(path_to_save, index=False)


tf.reset_default_graph()

batch_size = 512
steps_per_validation = 200
best_auc_score = 0.0

DATA_DIR = "./simple_gru/data/characterLvl"
MODELS_DIR = "./drive/character_lvl_rnn/models"
LOG_DIR = "./drive/character_lvl_rnn/logs"

sys.path.append("core/models")

with tf.Session() as sess:
    from TwoLayerLSTM import *
    init(session=sess, path=MODELS_DIR + '/character_lvl_rnn/model')
    writer = tf.summary.FileWriter(LOG_DIR, sess.graph)
    current_train_index = 1
    print('Submit started')
    make_submission(session=sess, path_to_file=DATA_DIR + '/test_char_set.csv.zip',
                    path_to_save=MODELS_DIR + '/test_submission.csv')

    writer.close()
