from gensim.models import Word2Vec
import pandas

# text1 = pandas.read_csv("./data/train1.csv")
# text2 = pandas.read_csv("./data/test1.csv")
# sentences = []
# for line in text1['comment_text']:
#     line = line.split('|')
#     sentences.append(line)
# row = 'start'
# for line in text2['comment_text']:
#     crow = line
#     try:
#         if '|' in line:
#             sentences.append(line.split('|'))
#         else:
#             if len(line) > 0:
#                 sentences.append([line, ])
#     except Exception as e:
#         print(row)
#     row = crow
#
# model = Word2Vec(sentences=sentences, size=100, window=5, min_count=10, workers=4, max_vocab_size=10000)
# model.save("./data/w2v")
# # model.build_vocab(sentences=sentences)
# for i in range(0, 30):
#     print("Step " + str(i) + " started")
#     model.train(sentences=sentences, total_examples=model.corpus_count, epochs=10)
#     model.save("./data/w2v")

model = Word2Vec.load("./data/w2v")
print(len(model.wv.vocab.keys()))
print(model.wv.vocab)
import numpy as np
voc = {"<PAD>": 0, "<UNK>": 1}
index = 2
matrix = [[0]*100, np.random.uniform(-1, 1, 10).tolist()]
for word in model.wv.vocab.keys():
    voc[word] = index
    index += 1
    matrix.append(model.wv[word].tolist())
print(index)
print(voc)
print(matrix[:10])
# while True:
#     try:
#         print('Input word:')
#         word = input()
#         print('Most similar to ' + '"' + word + '":')
#         res = model.wv.most_similar_cosmul(positive=[word])
#         for pair in res:
#             print(pair[0] + ' - ' + str(pair[1]))
#     except Exception as e:
#         print("----")

import pandas
import math
text = pandas.read_csv("./data/test1.csv")

def tokenize(s):
    if not isinstance(s, str) or s is None or len(s) == 0:
        return []
    else:
        if '|' not in s:
            s = [s, ]
        else:
            s = s.split('|')

        res = []
        for word in s:
            if word in voc.keys():
                res.append(voc[word])
            else:
                res.append(voc['<UNK>'])

        return res

text['comment_text'] = text['comment_text'].apply(tokenize)
for line in text['comment_text'][:20]:
    print(line)
    print('========================')
text.to_csv('./data/___test___tokinized.csv')