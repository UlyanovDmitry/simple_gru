import pandas
import numpy as np
text = pandas.read_csv("../data/new_data/train_tokenized.csv")[['comment_text','toxic','severe_toxic','obscene','threat','insult','identity_hate']]

valid_ratio = 0.3

total_count = len(text)
indexes = np.arange(total_count)
np.random.shuffle(indexes)
indexes = indexes.tolist()
print(indexes)

valid_count = int(total_count * valid_ratio)

text.ix[indexes[:valid_count]].to_csv('../data/new_data/valid_set_tokenized.csv', index=False)
text.ix[indexes[valid_count:]].to_csv('../data/new_data/train_set_tokenized.csv', index=False)