import numpy as np
import tensorflow as tf

predictions = tf.placeholder(shape=(None, None), dtype=tf.float32)
labels = tf.placeholder(shape=(None, None), dtype=tf.float32)

#indexes = tf.constant([1, 2, 0])
# slice_ = tf.expand_dims(indexes, 1)
# ind = tf.zeros([3], dtype=tf.int32)
# ind = tf.range(0, 3, 1, dtype=tf.int32)
# slice_ = tf.concat([tf.expand_dims(ind, 1), tf.expand_dims(indexes, 1)], 1)
# f = tf.gather_nd(t, slice_)  # [[[3, 3, 3]]]

# t = tf.constant([[1, 2, 3], [4, 5, 6]])
# _real_batch_size = tf.shape(t)[0]
# op = tf.slice(t, [0, 0], [_real_batch_size, 1])
#
# v = tf.get_variable(name="var", shape=1, initializer=tf.constant_initializer(3))
#
# with tf.variable_scope('AUC') as scope:
#     auc, update_op = tf.metrics.auc(labels=labels, predictions=predictions, curve='ROC')
#     _vars = tf.contrib.framework.get_variables(scope, collection=tf.GraphKeys.LOCAL_VARIABLES)
#     _auc_reset_op = tf.variables_initializer(_vars)


# auc3 = tf.map_fn(fn=lambda x: tf.metrics.auc(labels=x[1], predictions=x[0], curve='ROC')[0],
#                  elems=tf.concat([tf.expand_dims(predictions, 1),
#                                   tf.expand_dims(labels, 1)], 1), dtype=tf.float32)

# import pandas
# test_df = pandas.read_csv('../data/test_tokenized.csv', nrows=20, header=None, skiprows=1)
# print(test_df)

# def binary_activation(x):
#     cond = tf.less(x, tf.ones(tf.shape(x)))
#     out = tf.where(cond, tf.zeros(tf.shape(x)), tf.ones(tf.shape(x)))
#
#     return out
#
# f = binary_activation(predictions)

# with tf.Session() as sess:
#     # init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
#     # sess.run(init)
#     # fd = {predictions: [[1.0, 0.0]], labels: [[1.0, 0.0]]}
#     # _ = sess.run(update_op, feed_dict=fd)
#     # fd = {predictions: [[0.5, 0.5]], labels: [[0.0, 1.0]]}
#     # _ = sess.run(update_op, feed_dict=fd)
#     # score = sess.run(auc)
#     # print("Score0: {}".format(score))
#     #
#     # op = v.assign([33])
#     # print("V = {}".format(sess.run(op)))
#     #
#     # sess.run(_auc_reset_op)
#     #
#     # fd = {predictions: [[0.0, 1.0]], labels: [[1.0, 0.0]]}
#     # _ = sess.run(update_op, feed_dict=fd)
#     # score = sess.run(auc)
#     # print("Score1: {}".format(score))
#     # print("V = {}".format(sess.run(v)))
#     print(sess.run(op))
