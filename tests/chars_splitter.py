import pandas
import re

df = pandas.read_csv('./data/train.csv').sample(frac=1)
valid_size = int(0.3 * len(df))
df.ix[:valid_size - 1].to_csv('./data/valid_c.csv')
df.ix[valid_size:].to_csv('./data/train_c.csv')
train_df = pandas.read_csv('./data/train_c.csv')[['comment_text','toxic','severe_toxic','obscene','threat','insult','identity_hate']]
symbols = {}
total_number = 0
for row in train_df['comment_text']:
    s = re.sub('\.(\s*\.)+', '~', row)
    total_number += len(s)
    for c in s:
        if c in symbols.keys():
            symbols[c] += 1
        else:
            symbols[c] = 1

symbols = [tuple(x) for x in symbols.items()]
symbols.sort(key=lambda tup: -tup[1])
symbols = [x[0] for x in symbols[:127]]
print(total_number)
print(symbols)
print(len(symbols))

dictionary = {}
index = 1

for symbol in symbols:
    dictionary[symbol] = index
    index += 1

print(dictionary)



def tokenize(s):
    res = []
    ss = re.sub('\.(\s*\.)+', '~', s)
    for c in ss:
        if c in dictionary.keys():
            res.append(dictionary[c])
        else:
            res.append(0)
        if len(res) >= 512:
            return res
    return res

valid_df = pandas.read_csv('./data/valid_c.csv')[['comment_text','toxic','severe_toxic','obscene','threat','insult','identity_hate']]
valid_df['comment_text'] = valid_df['comment_text'].apply(tokenize)
train_df['comment_text'] = train_df['comment_text'].apply(tokenize)
test_df = pandas.read_csv('./data/test.csv')
test_df['comment_text'] = test_df['comment_text'].apply(tokenize)

valid_df.to_csv('./data/valid_char_set.csv', index=False)
train_df.to_csv('./data/train_char_set.csv', index=False)
test_df.to_csv('./data/test_char_set.csv', index=False)
