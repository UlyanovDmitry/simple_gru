import pandas
import re

nums1 = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
nums10 = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen']
nums2 = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety']

def normalize(content):
    content = content.lower()
    content = content.replace('can\'t', 'cannot')
    content = content.replace('i\'m', 'i am')
    content = content.replace('you\'re', 'you are')
    content = re.sub('n\'t', ' not', content)
    content = re.sub('[\n\"\t]', ' ', content)
    content = re.sub('[^\w\s?!;.,&\n\-]', '|', content)
    content = re.sub('\|\|+', ' @ ', content)
    content = re.sub('\s-', ' - ', content)
    content = re.sub('-\s', ' - ', content)
    content = re.sub('\.(\s*\.)+', ' _ ', content)
    #content = content.replace('\.\.+', ' _ ')
    content = content.replace('.', ' . ')
    content = content.replace(',', ' , ')
    content = content.replace(';', ' ; ')
    content = content.replace('?', ' ? ')
    content = content.replace('!', ' ! ')
    content = re.sub('\A\s+', '', content)
    content = re.sub('\s+\Z', '', content)
    content = re.sub('\s\s+', ' ', content)
    content = re.sub('\s', '|', content)
    content = re.sub('\|\|+', '|', content)
    content = content.replace('_', '...')
    return content

text = pandas.read_csv("../data/train.csv")
text['comment_text'] = text['comment_text'].apply(normalize)
for line in text['comment_text'][:20]:
    print(line)
    print('========================')
text.to_csv('../data/train1.csv')

text = pandas.read_csv("../data/test.csv")
text['comment_text'] = text['comment_text'].apply(normalize)
for line in text['comment_text'][:20]:
    print(line)
    print('========================')
text.to_csv('../data/test1.csv')