import numpy as np
import tensorflow as tf
from tensorflow.contrib import layers
import random

from tensorflow.python.layers import core as layers_core

vocab_size = 100
embedding_size = 20

batch_size = 150
num_units = 20

learning_rate = 0.1
max_gradient_norm = 10.0

num_of_classes = 6

inputs = tf.placeholder(shape=(None,  None), dtype=tf.int32, name='inputs')
print(inputs)
oh_inputs = tf.one_hot(inputs, vocab_size)
target = tf.placeholder(shape=(None, num_of_classes), dtype=tf.float32, name='target')
print(target)
# init = tf.constant(shape=(vocab_size, embedding_size), tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0))
embedding = tf.get_variable("embedding", initializer=tf.random_uniform(shape=(vocab_size, embedding_size), minval=-1.0, maxval=1.0), dtype=tf.float32, trainable=True)
emb_inputs = tf.nn.embedding_lookup(embedding, inputs, name="emb_inputs")
# emb_inputs = tf.matmul(oh_inputs, embedding)
print(embedding)
print(oh_inputs)

encoder_cell = tf.contrib.rnn.GRUCell(num_units=num_units)

encoder_outputs, encoder_state = tf.nn.dynamic_rnn(
    encoder_cell, emb_inputs, time_major=False, dtype=tf.float32)

print(encoder_outputs)

fc_layer = layers.fully_connected(inputs=encoder_outputs[:, -1, :], num_outputs=num_of_classes, activation_fn=None)

print(fc_layer)

loss = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(logits=fc_layer, labels=target, name='loss')) / (num_of_classes * batch_size)

params = tf.trainable_variables()

gradients = tf.gradients(loss, params)
clipped_gradients, _ = tf.clip_by_global_norm(
    gradients, max_gradient_norm)

tf.summary.histogram('histogram', loss)
merged = tf.summary.merge_all()
print(loss)

train_op = tf.train.AdamOptimizer(learning_rate)

update_step = train_op.apply_gradients(
    zip(clipped_gradients, params))

###########################

def next_feed():
    max_sequence_length = random.randint(8, 10)
    _inputs = np.zeros(shape=[batch_size, max_sequence_length], dtype=np.int32)
    _target = np.zeros(shape=[batch_size, num_of_classes], dtype=np.float32)
    for i in range(0, batch_size):
        sequence_length = random.randint(6, max_sequence_length)
        for j in range(0, sequence_length):
            el = random.randint(1, vocab_size-1)
            _inputs[i][j] = el
            for k in range(0, num_of_classes):
                if el % (k+2) == 0:
                    _target[i][k] += 1
        for p in range(0, num_of_classes):
            if _target[i][p] > sequence_length/2:
                _target[i][p] = 1.0
            else:
                _target[i][p] = 0.0
    return {
        inputs: _inputs,
        target: _target
    }

loss_track = []

max_batches = 2001
batches_in_epoch = 1000
saver = tf.train.Saver()

with tf.Session(config=tf.ConfigProto(device_count={'GPU':0})) as sess:
    sess.run(tf.global_variables_initializer())
    writer = tf.summary.FileWriter("logs", sess.graph)
    try:
        saver.restore(sess, "./models/mode12")
    except Exception as e:
        print("Cannot be read")
    for batch in range(max_batches):
        fd = next_feed()
        _, l, summary, fc = sess.run([update_step, loss, merged, fc_layer], fd)
        writer.add_summary(summary, batch)
        loss_track.append(l)

        if batch == 0 or batch % batches_in_epoch == 0:
            print('batch {}'.format(batch))
            print('  minibatch loss: {}'.format(sess.run(loss, fd)))
            print('  input: {}'.format(fd[inputs][0]))
            print('  target: {}'.format(fd[target][0]))
            print('  predicted: {}'.format(fc[0]))
            print('  predicted_s: {}'.format(sess.run(tf.sigmoid(fc[0]))))
            # predict_ = sess.run(decoder_prediction, fd)
            # for i, (inp, pred) in enumerate(zip(fd[encoder_inputs].T, predict_.T)):
            #     print('  sample {}:'.format(i + 1))
            #     print('    input     > {}'.format(inp))
            #     print('    predicted > {}'.format(pred))
            #     if i >= 2:
            #         break
            save_path = saver.save(sess, "./models/mode12")
            print("Saved at " + save_path)
            print()
    writer.close()

    import matplotlib.pyplot as plt
    plt.plot(loss_track)
    plt.show()
    #print('loss {:.4f} after {} examples (batch_size={})'.format(loss_track[-1], len(loss_track)*batch_size, batch_size))

    while True:
        print("Input sequence:")
        s = [int(a) for a in input().split(" ")]
        predict = sess.run([fc_layer], {inputs: [s, ]})
        print(sess.run(tf.sigmoid(predict[0])))