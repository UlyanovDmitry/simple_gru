import pandas
from ast import literal_eval
import numpy as np

train_set_size = 127657
valid_set_size = 15957
test_set_size = 15957

train_current_index = 0
valid_current_index = 0
test_current_index = 0

def get_next_train_rows(number_of_rows):
    global train_current_index
    global train_set_size
    text = pandas.read_csv("./data/train_set_tokenized.csv", skiprows=train_current_index, nrows=number_of_rows)
    train_current_index += number_of_rows
    if train_current_index >= train_set_size:
        train_current_index = 0
    d = text[['comment_text', 'toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate']].to_dict()
    res = []
    for i in range(0, len(d['comment_text'])):
        res.append([literal_eval(d['comment_text'][i]), [d['toxic'][i], d['severe_toxic'][i], d['obscene'][i], d['threat'][i], d['insult'][i], d['identity_hate'][i]]])
    return res

def create_batch(batch_size):
    data = get_next_train_rows(batch_size)
    max_sequence_length = len(max(data, key=lambda x: len(x[0]))[0])
    _inputs = np.zeros(shape=[batch_size, max_sequence_length], dtype=np.int32)
    _target = np.zeros(shape=[batch_size, len(data[0][1])], dtype=np.float32)
    _lengths = np.zeros(shape=[batch_size], dtype=np.int32)
    for i in range(0, len(data)):
        _lengths[i] = len(data[i][0])
        for j in range(0, len(data[i][0])):
            _inputs[i][j] = data[i][0][j]
        for k in range(0, len(data[i][1])):
            _target[i][k] = data[i][1][k]
    return [_inputs, _target, _lengths]


df = pandas.read_csv('./data/my_submission.csv').drop(['Unnamed: 0'],axis=1)
df.to_csv('./data/my_submission.csv', index=False)
