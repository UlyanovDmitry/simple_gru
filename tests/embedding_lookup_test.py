import numpy as np
import tensorflow as tf

vocab_size = 4
embedding_size = 3

batch_size = 2
num_units = 20

tf.reset_default_graph()
sess = tf.InteractiveSession(config=tf.ConfigProto(device_count={'GPU':0}))

inputs = tf.placeholder(shape=(batch_size,  None), dtype=tf.int32, name='inputs')

oh_inputs = tf.one_hot(inputs, vocab_size, dtype=tf.int32)

embedding = tf.placeholder(shape=(vocab_size, embedding_size), dtype=tf.int32, name="embedding")
emb_inputs = tf.nn.embedding_lookup(embedding, inputs, name="emb_inputs")

sess.run(tf.global_variables_initializer())

emb_inputsi, emb, e_i = sess.run([oh_inputs, embedding, emb_inputs], feed_dict={inputs: [[2, 1], [3, 0]], embedding: [[1, 2, 3], [4, 5, 6], [7, 8, 9], [0, 1, 2]]})

print(emb_inputsi)
print()
print(embedding)
print()
print(e_i)