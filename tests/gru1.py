import numpy as np
import tensorflow as tf
from tensorflow.contrib import layers
import random

LOG_DIR = 'logs'
MODEL_SAVING_DIR = './models/gru/'
DATA_DIR = './data/'

vocab_size = 3183
embedding_size = 100
num_units = 100

learning_rate = 0.001
max_gradient_norm = 5.0

num_of_classes = 6


def init_embedding():
    from gensim.models import Word2Vec
    model = Word2Vec.load("./data/w2v")
    vectors = model.wv
    del model
    embeddings = np.zeros(shape=[vocab_size, embedding_size], dtype=np.float32)
    embeddings[1] = np.random.uniform(-1, 1, 100)
    i = 2
    for word in vectors.wv.vocab.keys():
        embeddings[i] = vectors.wv[word]
        i += 1
    return embeddings

def binary_activation(x):
    cond = tf.less(x, tf.zeros(tf.shape(x)))
    out = tf.where(cond, tf.zeros(tf.shape(x)), tf.ones(tf.shape(x)))

    return out

tf.reset_default_graph()

inputs = tf.placeholder(shape=(None,  None), dtype=tf.int32, name='inputs')
inputs_length = tf.placeholder(shape=(None, ), dtype=tf.int32, name='inputs_length')
target = tf.placeholder(shape=(None, num_of_classes), dtype=tf.float32, name='target')

real_batch_size = tf.shape(inputs_length)[0] #tf.reshape(tf.shape(inputs_length)[0], [])
print(real_batch_size)

indexes = tf.range(0, real_batch_size, 1, dtype=tf.int32)
output_slice = tf.concat([tf.expand_dims(indexes, 1), tf.expand_dims(inputs_length, 1)], 1)

embedding = tf.get_variable("embedding", initializer=init_embedding(), dtype=tf.float32, trainable=True)
emb_inputs = tf.nn.embedding_lookup(embedding, inputs, name="emb_inputs")

encoder_cell = tf.contrib.rnn.GRUCell(num_units=num_units)

encoder_outputs, encoder_state = tf.nn.dynamic_rnn(
    encoder_cell, emb_inputs, time_major=False, dtype=tf.float32)

fully_connected_input = tf.gather_nd(encoder_outputs, output_slice)

fully_connected_layer = layers.fully_connected(inputs=fully_connected_input, num_outputs=num_of_classes, activation_fn=None)

train_loss = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(logits=fully_connected_layer, labels=target, name='loss')) / (num_of_classes * 150)

hard_prediction = binary_activation(fully_connected_layer)
smooth_prediction = tf.sigmoid(fully_connected_layer)

tf.summary.scalar('train_loss', train_loss)

params = tf.trainable_variables()

gradients = tf.gradients(train_loss, params)
clipped_gradients, _ = tf.clip_by_global_norm(
    gradients, max_gradient_norm)

global_step = tf.get_variable('global_step', initializer=0, trainable=False)
train_op = tf.train.AdamOptimizer(learning_rate)

update_step = train_op.apply_gradients(
    zip(clipped_gradients, params), global_step=global_step)

merged = tf.summary.merge_all()

###############################################

batch_size = 16
max_batches = 301
batches_in_epoch = 100
loss_track = []


import pandas
from ast import literal_eval
import numpy as np

train_set_size = 127657
valid_set_size = 15957
test_set_size = 15957

train_current_index = 1
valid_current_index = 1
test_current_index = 1

def get_tokenized_rows(index_from, number_of_rows, path):
    text = pandas.read_csv(path, skiprows=index_from, nrows=number_of_rows)
    d = text.iloc[:, 4:11]
    res = []
    for i in range(0, len(d.iloc[:, 0])):
        res.append([literal_eval(d.iloc[i, 0]),
                    [d.iloc[i, 1], d.iloc[i, 2], d.iloc[i, 3], d.iloc[i, 4], d.iloc[i, 5], d.iloc[i, 6]]])
    return res

def _get_train_rows():
    text = pandas.read_csv('./data/___test___tokinized.csv', skiprows=1)
    d = text.iloc[:, 3:4]
    res = []
    for i in range(0, len(d.iloc[:, 0])):
        res.append(literal_eval(d.iloc[i, 0]))
    return res

def get_next_train_rows(number_of_rows):
    global train_current_index
    global train_set_size
    res = get_tokenized_rows(train_current_index, number_of_rows, DATA_DIR + 'train_set_tokenized.csv')
    train_current_index += number_of_rows
    if train_current_index >= train_set_size:
        train_current_index = 1
    return res

def get_next_valid_rows(number_of_rows):
    global valid_current_index
    global valid_set_size
    res = get_tokenized_rows(valid_current_index, number_of_rows, DATA_DIR + 'train_set_tokenized.csv')
    valid_current_index += number_of_rows
    if valid_current_index >= valid_set_size:
        valid_current_index += 1
    return res

def create_batch(batch_size):
    data = get_next_train_rows(batch_size)
    max_sequence_length = len(max(data, key=lambda x: len(x[0]))[0])
    _inputs = np.zeros(shape=[batch_size, max_sequence_length], dtype=np.int32)
    _target = np.zeros(shape=[batch_size, len(data[0][1])], dtype=np.float32)
    _lengths = np.zeros(shape=[batch_size], dtype=np.int32)
    for i in range(0, len(data)):
        _lengths[i] = len(data[i][0])-1
        for j in range(0, len(data[i][0])):
            _inputs[i][j] = data[i][0][j]
        for k in range(0, len(data[i][1])):
            _target[i][k] = data[i][1][k]
    return [_inputs, _target, _lengths]


def next_feed():
    feed = create_batch(batch_size)
    return {
        inputs: feed[0],
        target: feed[1],
        inputs_length: feed[2]
    }
#####################################

saver = tf.train.Saver()

with tf.Session(config=tf.ConfigProto(device_count={'GPU':0})) as sess:
    sess.run(tf.global_variables_initializer())
    writer = tf.summary.FileWriter(LOG_DIR, sess.graph)
    try:
        saver.restore(sess, MODEL_SAVING_DIR + "model")
    except Exception as e:
        print(e)
        print("Cannot be read")

    #################
    # for batch in range(max_batches):
    #     fd = next_feed()
    #     _, l, summary, fc, g_step = sess.run([update_step, train_loss, merged, fully_connected_layer, global_step], fd)
    #     writer.add_summary(summary, global_step=g_step)
    #     loss_track.append(l)
    #
    #     if batch == 0 or batch % batches_in_epoch == 0:
    #         print('batch {}'.format(g_step))
    #         print('  minibatch loss: {}'.format(l))
    #         print('  input: {}'.format(fd[inputs][0]))
    #         print('  target: {}'.format(fd[target][0]))
    #         print('  predicted: {}'.format(fc[0]))
    #         print('  predicted_s: {}'.format(sess.run(tf.sigmoid(fc[0]))))
    #         # predict_ = sess.run(decoder_prediction, fd)
    #         # for i, (inp, pred) in enumerate(zip(fd[encoder_inputs].T, predict_.T)):
    #         #     print('  sample {}:'.format(i + 1))
    #         #     print('    input     > {}'.format(inp))
    #         #     print('    predicted > {}'.format(pred))
    #         #     if i >= 2:
    #         #         break
    #         save_path = saver.save(sess, MODEL_SAVING_DIR + "model")
    #         print("Saved at " + save_path)
    #         print()
    # writer.close()
    #
    # import matplotlib.pyplot as plt
    #
    # plt.plot(loss_track)
    # plt.show()
    # print('loss {:.4f} after {} examples (batch_size={})'.format(loss_track[-1], len(loss_track)*batch_size, batch_size))


    l = _get_train_rows()
    submission = pandas.read_csv('./data/sample_submission.csv')
    i = 0
    for row in l:
        if len(row) == 0:
            row.append(0)
        predict = sess.run([smooth_prediction, ], {inputs: [row, ], inputs_length: [len(row)-1, ]})
        for j in range(0, 6):
            submission.iloc[i, j + 1] = predict[0].tolist()[0][j]
        i += 1
        print('Row {} completed'.format(i))


    # for i in range(0, len(results)):
    #     for j in range(0, 6):
    #         submission.iloc[i, j + 1] = results[i][0][j]
    submission.to_csv('./data/my_submission.csv')

    # while True:
    #     print("Input sequence:")
    #     s = [int(a) for a in input().split(", ")]
    #     predict, s_predict = sess.run([hard_prediction, smooth_prediction], {inputs: [s, ], inputs_length: [len(s)-1,]})
    #     print(predict)
    #     print(s_predict)