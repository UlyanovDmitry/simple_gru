# import json
#
# import json
#
# data = {
#    'name' : 'ACME',
#    'shares' : 100,
#    'price' : 542.23
# }
#
# json_str = json.dumps(data)
#
# data = json.loads(json_str)
#
# # Writing JSON data
# with open('data.json', 'w') as f:
#      json.dump(data, f)
#
# # Reading data back
# with open('data.json', 'r') as f:
#      data = json.load(f)
#      print(data['price'])

import pandas

df= pandas.read_csv('../data/train_set_tokenized.csv')
df = df.sample(frac=1)
print(df)