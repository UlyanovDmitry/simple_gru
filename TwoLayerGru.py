import tensorflow as tf
from word2vec import get_dict_from_model
from tensorflow.contrib import layers

learning_rate = 0.00005
max_gradient_norm = 3.0
num_of_units = 200
num_of_classes = 6

_, m = get_dict_from_model('./simple_gru/data/word2vec')

_inputs = tf.placeholder(shape=(None,  None), dtype=tf.int32, name='inputs')
_inputs_length = tf.placeholder(shape=(None, ), dtype=tf.int32, name='inputs_length')
_target = tf.placeholder(shape=(None, num_of_classes), dtype=tf.float32, name='target')

_real_batch_size = tf.shape(_inputs_length)[0]

with tf.variable_scope("embedding"):
    _embedding = tf.get_variable("matrix", shape=(len(m), len(m[0])),
                                 initializer=tf.constant_initializer(m),
                                 dtype=tf.float32, trainable=True)
    _emb_inputs = tf.nn.embedding_lookup(_embedding, _inputs, name="lookup")

with tf.variable_scope("RNN"):
    with tf.variable_scope("LAYER_0"):
        _encoder_cell_fw_0 = tf.contrib.rnn.GRUCell(num_units=num_of_units/2)
        _encoder_cell_bw_0 = tf.contrib.rnn.GRUCell(num_units=num_of_units/2)
        _ouputs_0, _final_state_0 = tf.nn.bidirectional_dynamic_rnn(cell_fw=_encoder_cell_fw_0,
                                                                    cell_bw=_encoder_cell_bw_0,
                                                                    inputs=_emb_inputs,
                                                                    sequence_length=_inputs_length,
                                                                    time_major=False, dtype=tf.float32)

        _layer_0_output = tf.concat(_ouputs_0, 2)

    with tf.variable_scope("LAYER_1"):
        _encoder_cell_1 = tf.contrib.rnn.GRUCell(num_units=num_of_units)
        _ouputs_1, _final_state_1 = tf.nn.dynamic_rnn(cell=_encoder_cell_1,
                                                      inputs=_layer_0_output,
                                                      sequence_length=_inputs_length,
                                                      time_major=False, dtype=tf.float32)

with tf.variable_scope("FC"):
    _fully_connected_input = _final_state_1
    _fully_connected_layer = layers.fully_connected(inputs=_fully_connected_input,
                                                    num_outputs=num_of_classes, activation_fn=None)
with tf.variable_scope("LOSS"):
    _train_loss = tf.reduce_sum(
        tf.nn.sigmoid_cross_entropy_with_logits(logits=_fully_connected_layer, labels=_target,
                                                name='loss')) / tf.cast(_real_batch_size, tf.float32)

    _prediction = tf.sigmoid(_fully_connected_layer)


with tf.variable_scope('AUC') as scope:
    _auc0, _auc_op0 = tf.metrics.auc(labels=tf.slice(_target, [0, 0], [_real_batch_size, 1]),
                                     predictions=tf.slice(_prediction, [0, 0], [_real_batch_size, 1]))
    _auc1, _auc_op1 = tf.metrics.auc(labels=tf.slice(_target, [0, 1], [_real_batch_size, 1]),
                                     predictions=tf.slice(_prediction, [0, 1], [_real_batch_size, 1]))
    _auc2, _auc_op2 = tf.metrics.auc(labels=tf.slice(_target, [0, 2], [_real_batch_size, 1]),
                                     predictions=tf.slice(_prediction, [0, 2], [_real_batch_size, 1]))
    _auc3, _auc_op3 = tf.metrics.auc(labels=tf.slice(_target, [0, 3], [_real_batch_size, 1]),
                                     predictions=tf.slice(_prediction, [0, 3], [_real_batch_size, 1]))
    _auc4, _auc_op4 = tf.metrics.auc(labels=tf.slice(_target, [0, 4], [_real_batch_size, 1]),
                                     predictions=tf.slice(_prediction, [0, 4], [_real_batch_size, 1]))
    _auc5, _auc_op5 = tf.metrics.auc(labels=tf.slice(_target, [0, 5], [_real_batch_size, 1]),
                                     predictions=tf.slice(_prediction, [0, 5], [_real_batch_size, 1]))

    _average_auc = (_auc0 + _auc1 + _auc2 + _auc3 + _auc4 + _auc5) / 6

    _vars = tf.contrib.framework.get_variables(scope, collection=tf.GraphKeys.LOCAL_VARIABLES)
    _auc_reset_op = tf.variables_initializer(_vars)

tf.summary.scalar('loss', _train_loss, collections=['per_step'])
tf.summary.scalar('auc_toxic', _auc0, collections=['per_epoch'])
tf.summary.scalar('auc_severe_toxic', _auc1, collections=['per_epoch'])
tf.summary.scalar('auc_obscene', _auc2, collections=['per_epoch'])
tf.summary.scalar('auc_threat', _auc3, collections=['per_epoch'])
tf.summary.scalar('auc_insult', _auc4, collections=['per_epoch'])
tf.summary.scalar('auc_identity_hate', _auc5, collections=['per_epoch'])
tf.summary.scalar('average_auc', _average_auc, collections=['per_epoch'])

_params = tf.trainable_variables()

_gradients = tf.gradients(_train_loss, _params)
_clipped_gradients, _ = tf.clip_by_global_norm(
    _gradients, max_gradient_norm)

_global_step = tf.get_variable('global_step', initializer=0, trainable=False)
_train_op = tf.train.AdamOptimizer(learning_rate)

update_step = _train_op.apply_gradients(
    zip(_clipped_gradients, _params), global_step=_global_step)

_merged_per_step = tf.summary.merge_all(key='per_step')
_merged_per_epoch = tf.summary.merge_all(key='per_epoch')

saver = tf.train.Saver()


def init(session, path):
    _init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    session.run(_init)
    try:
        saver.restore(session, path)
    except Exception as e:
        print(e)
        print("Cannot be read")


def save(session, path):
    save_path = saver.save(session, path)
    print("Saved at " + save_path)


def train(session, inputs, input_lengths, targets, writer=None):
    feed_dict = {
        _inputs: inputs,
        _inputs_length: input_lengths,
        _target: targets
    }
    loss, _, summary, global_step = session.run([_train_loss, update_step, _merged_per_step, _global_step], feed_dict)
    if writer is not None:
        writer.add_summary(summary, global_step=global_step)
    return loss


def reset_auc(session):
    session.run(_auc_reset_op)


def validate(session, inputs, input_lengths, targets):
    feed_dict = {
        _inputs: inputs,
        _inputs_length: input_lengths,
        _target: targets
    }
    session.run([_auc_op0, _auc_op1, _auc_op2, _auc_op3, _auc_op4, _auc_op5], feed_dict)


def get_auc(session, writer=None):
    auc, summary, global_step = session.run([_average_auc, _merged_per_epoch, _global_step])
    if writer is not None:
        writer.add_summary(summary, global_step=global_step)
    return auc


def predict(session, inputs, input_lengths):
    feed_dict = {
        _inputs: inputs,
        _inputs_length: input_lengths
    }
    pred = session.run(_prediction, feed_dict)
    return pred
