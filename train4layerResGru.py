#!/usr/bin/python
from ast import literal_eval
import numpy as np
import tensorflow as tf
import pandas
import sys


def get_tokenized_rows(path, index_from, max_number_of_rows):
    d = pandas.read_csv(path, skiprows=index_from, header=None, nrows=max_number_of_rows)
    res = []
    for i in range(0, len(d.iloc[:, 0])):
        res.append([literal_eval(d.iloc[i, 0]),
                    [d.iloc[i, 1], d.iloc[i, 2], d.iloc[i, 3], d.iloc[i, 4], d.iloc[i, 5], d.iloc[i, 6]]])
    return res, len(d)


def create_batch(path, index_from, max_number_of_rows):
    _data, _len = get_tokenized_rows(path=path, index_from=index_from, max_number_of_rows=max_number_of_rows)
    _max_sequence_length = len(max(_data, key=lambda x: len(x[0]))[0])
    _inputs = np.zeros(shape=[max_number_of_rows, _max_sequence_length], dtype=np.int32)
    _target = np.zeros(shape=[max_number_of_rows, len(_data[0][1])], dtype=np.float32)
    _lengths = np.zeros(shape=[max_number_of_rows], dtype=np.int32)
    for i in range(0, len(_data)):
        _lengths[i] = len(_data[i][0])
        for j in range(0, len(_data[i][0])):
            _inputs[i][j] = _data[i][0][j]
        for k in range(0, len(_data[i][1])):
            _target[i][k] = _data[i][1][k]
    return _inputs, _target, _lengths, _len


def make_submission(session, path_to_file, path_to_save):
    result = pandas.DataFrame(columns=['id', 'toxic', 'severe_toxic', 'obscene', 'threat', 'insult', 'identity_hate'])
    current_index = 1
    finished = False
    _nrows = 1024
    while not finished:
        test_df = pandas.read_csv(path_to_file, nrows=_nrows, header=None, skiprows=current_index)
        if len(test_df) < _nrows:
            finished = True
        _data = [literal_eval(row) for row in test_df.iloc[:, 1].tolist()]
        _ids = test_df.iloc[:, 0].tolist()
        _max_sequence_length = len(max(_data, key=lambda x: len(x)))
        _inputs = np.zeros(shape=[len(test_df), _max_sequence_length], dtype=np.int32)
        _lengths = np.zeros(shape=[len(test_df)], dtype=np.int32)
        for i in range(0, len(_data)):
            _lengths[i] = len(_data[i])
            for j in range(0, len(_data[i])):
                _inputs[i][j] = _data[i][j]

        _prediction = predict(session=session, inputs=_inputs, input_lengths=_lengths)

        for k in range(len(test_df)):
            result.loc[k + current_index - 1] = [_ids[k], _prediction[k][0], _prediction[k][1], _prediction[k][2],
                                                 _prediction[k][3], _prediction[k][4], _prediction[k][5]]
        current_index += len(test_df)
        print("Submission rows complete: {}".format(current_index - 1))

    result.to_csv(path_to_save, index=False)


tf.reset_default_graph()

batch_size = 48
valid_batch_size = 512
steps_per_validation = 200
steps_per_save = 100
best_auc_score = 0.0
current_train_index = 1


def shuffle_train_set():
    df = pandas.read_csv(DATA_DIR + '/train_set_tokenized.csv').sample(frac=1)
    df.to_csv(DATA_DIR + '/train_set_tokenized.csv', index=False)


def load_init_values():
    import json
    global best_auc_score, current_train_index
    with open(MODELS_DIR + '/global_vars.json', 'r') as file:
        data = json.load(file)
        best_auc_score = data['best_auc_score']
        current_train_index = data['current_train_index']


def save_values():
    import json
    data = {
        'best_auc_score': float(str(best_auc_score)),
        'current_train_index': current_train_index
    }
    with open(MODELS_DIR + '/global_vars.json', 'w') as f:
        json.dump(data, f)


DATA_DIR = "./simple_gru/data"
MODELS_DIR = "./drive/four_layer_res_gru/models"
LOG_DIR = "./drive/four_layer_res_gru/logs"

sys.path.append("core/models")

with tf.Session() as sess:
    from FourLayerResGru import *

    init(session=sess, path=MODELS_DIR + '/four_layer_res_gru/model')
    writer = tf.summary.FileWriter(LOG_DIR, sess.graph)
    load_init_values()
    save_values()
    shuffle_train_set()
    print('Training started')
    for step in range(10000):
        inp, tar, lens, n_train_rows = create_batch(path=DATA_DIR + '/train_set_tokenized.csv',
                                                    index_from=current_train_index, max_number_of_rows=batch_size)
        loss = train(session=sess, inputs=inp, input_lengths=lens, targets=tar, writer=writer)

        current_train_index += n_train_rows

        print("Step {}. Batch loss: {}".format(step, loss))

        if n_train_rows < batch_size:
            current_train_index = 1
            print("Epoch ends")
            shuffle_train_set()

        if step != 0 and step % steps_per_save == 0:
            save(session=sess, path=MODELS_DIR + '/four_layer_res_gru/model')
            save_values()

        if n_train_rows < batch_size or step % steps_per_validation == 0:
            current_valid_index = 1
            print("Validation started")
            reset_auc(session=sess)
            n_valid_rows = valid_batch_size
            while n_valid_rows == valid_batch_size:
                inp, tar, lens, n_valid_rows = create_batch(path=DATA_DIR + '/valid_set_tokenized.csv',
                                                            index_from=current_valid_index,
                                                            max_number_of_rows=valid_batch_size)
                validate(session=sess, inputs=inp, input_lengths=lens, targets=tar)
                print("Validation index: {}.".format(current_valid_index))
                current_valid_index += n_valid_rows

            auc_score = get_auc(session=sess, writer=writer)
            print('Auc score: {}'.format(auc_score))
            if auc_score > best_auc_score:
                print('New best score reached: {}'.format(auc_score))
                best_auc_score = auc_score
                save(session=sess, path=MODELS_DIR + '/bestScore/model')

                # make_submission(session=sess, path_to_file=DATA_DIR + '/test_tokenized.csv',
                #                 path_to_save=MODELS_DIR + '/test_submission.csv')

                print("Saved!")

    writer.close()
