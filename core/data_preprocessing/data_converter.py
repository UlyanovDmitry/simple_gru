import re, string

available_symbols = r"[^\w\s?!;.,\-\n]"

spacer = '|'

re_tok = re.compile(f'([{string.punctuation}“”¨«»®´·º½¾¿¡§£₤‘’])')

def normalize(l):
    l = re.sub('\.(\s*\.)+', '~', l)
    l = l.replace("'", '')
    res = re_tok.sub(r' \1 ', l.lower()).split()
    return [w.replace('~', '...') for w in res]


def tokenize(row, dictionary, default_token):
    from ast import literal_eval
    words = literal_eval(row)
    # if row is None or not isinstance(row, str):
    #     return [default_token, ]
    # else:
    #     words = row.split(spacer)
    #     res = []
    res = []
    if len(words) >0:
        for word in words:
            if word in dictionary.keys():
                res.append(dictionary[word])
            else:
                res.append(default_token)
        return res
    else:
        return [default_token, ]

# import pandas
# text = pandas.read_csv("../../data/test.csv")
# text['comment_text'] = text['comment_text'].apply(normalize)
# for line in text['comment_text'][:20]:
#     print(line)
#     print('========================')
# text.to_csv('../../data/new_data/test_splitted.csv', index=False)